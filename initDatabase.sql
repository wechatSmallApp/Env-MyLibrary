#step 1: Create the Database
create database wechat;

#step 2: Verify that its there
show databases;

#step 3: Create the User
create user wechat;

#step 4: Grant privileges while assigning the password
grant all on wechat. * to 'wechat' identified by 'wechat';
grant select, insert, delete, update on wechat.* to 'wechat'@'localhost' identified by 'wechat';