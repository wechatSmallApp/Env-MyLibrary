# <a name="top"></a>MyLibrary Env

## Introduction

This is environment configuration for MyLibrary project.

## Structure
For example: <br>
Env-MyLibrary <br>
&nbsp;&nbsp; | <br>
&nbsp;&nbsp; |--docker-compose.yml <br>
&nbsp;&nbsp; |--initDatabase.sql <br>
&nbsp;&nbsp; |--README.md <br>

## Download docker image
>docker pull tomcat:9.0-jre8-alpine
>docker pull mysql

## Run docker compose
Before the run, create your own volumns, here mine is ../sharedata/, just example.
In folder Env-MyLibrary, launch docker images:
>docker-compose up

## Create database and user
Connect mysql: jdbc:mysql://localhost:3306
Using initDatabase.sql to create database and user.
